FROM ubuntu:22.04 as build_stage
WORKDIR /findent-octopus
COPY . .

RUN sed -e 's/archive.ubuntu.com/ftp.gwdg.de\/pub\/linux\/debian/' -i /etc/apt/sources.list && \
    apt-get update && apt-get -y install bison flex make gcc g++
RUN make -C ./bin


FROM ubuntu:22.04
ENV PATH="/findent-octopus:${PATH}"
WORKDIR /findent-octopus
COPY --from=build_stage /findent-octopus/bin/findent-octopus ./
COPY --from=build_stage /findent-octopus/scripts/ ./
RUN sed -e 's/archive.ubuntu.com/ftp.gwdg.de\/pub\/linux\/debian/' -i /etc/apt/sources.list && \
    apt-get update && apt-get -y install git git-lfs



